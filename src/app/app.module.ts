import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ContactsListComponent } from './contacts-list/contacts-list.component';
import { ContactsManagerComponent } from './contacts-manager/contacts-manager.component';

// Router
import { RouterModule, Routes } from '@angular/router';


// Database connections
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { HeaderComponent } from './header/header.component';
import { ContactFormComponent } from './contacts-manager/contact-form/contact-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProviderService } from './provider.service';
import { AuthComponent } from './auth/auth.component';
import { AuthService } from './auth.service';
import { AngularFireAuthModule } from 'angularfire2/auth';

// Router pages
const appRoutes: Routes = [
    {path: '', redirectTo: 'auth', pathMatch: 'full'},
    {path: 'auth', component: AuthComponent},
    {path: 'contacts-list', component: ContactsListComponent},
    {path: 'manage-contact', component: ContactsManagerComponent},
    {path: 'contact-form', component: ContactFormComponent}
];

@NgModule({
    declarations: [
        AppComponent,
        ContactsListComponent,
        ContactsManagerComponent,
        HeaderComponent,
        ContactFormComponent,
        AuthComponent
    ],
    imports: [
        BrowserModule,
        // Database setup
        AngularFireModule.initializeApp({
            apiKey: 'AIzaSyCdw5dLBdvk8bGH5Wwk1jQ-4tvrq8ZcsgE',
            authDomain: 'gig-project-8a119.firebaseapp.com',
            databaseURL: 'https://gig-project-8a119.firebaseio.com',
            projectId: 'gig-project-8a119',
            storageBucket: 'gig-project-8a119.appspot.com',
            messagingSenderId: '467499924762'
        }, 'Gig-project'),
        AngularFireDatabaseModule,
        // Authentication module of firebase
        AngularFireAuthModule,
        //  Router
        RouterModule.forRoot(
            appRoutes,
            {enableTracing: true}
        ),
        ReactiveFormsModule
    ],
    providers: [ProviderService, AuthService],
    bootstrap: [AppComponent]
})
export class AppModule {
}

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
import { ProviderService } from '../provider.service';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

    // Form vars
    signForm; // The FromGroup object var who will keep the form configuration
    formMode = 'signin'; // Default is 'add' mode
    formSetedUp = false; // To display the html form after the 'FormGroup' is ready in here

    msg = ''; // Feedback message

    constructor(private auth: AuthService,
                private _firebaseAuth: AngularFireAuth,
                private provider: ProviderService,
                private route: Router) {
        // Here we check user session (logged in or logged out) to display app or login form
        this._firebaseAuth.authState.subscribe((data) => {
            console.log('auth state', data);
            // Auth keeps user session alive
            if (data) {
                this.auth.loginUser(); // Telling the app we are online
                this.auth.setUser(data); // Storing user data in a var to user it in others operations
                this.provider.setContactPath('/users/' + this.auth.getUser().uid + '/contacts'); // path to get/set user's contacts from db
                this.route.navigate(['/contacts-list']); // Going to app main page
            } else {
                this.setForm();
            }
        });
    }

    ngOnInit() {
    }

    /**
     * Submit method
     */
    onSubmit() {
        const values = this.signForm.value;
        const user = {email: values.email, password: values.password};
        console.log('user', user);

        // Cheking form mode (add || edit)
        if (this.formMode === 'signin') {
            // Here code to signin
            this.signIn(user.email, user.password);
        } else {
            // Here code to signup
            this.signUp(user.email, user.password);
        }
    }

    /**
     * Set the form according of the mode that user is coming ('add' or 'edit')
     */
    setForm() {
        // Edit mode
        if (this.formMode === 'signup') {
            this.setFormToSignup();

            // Adding mode
        } else {
            this.setFormToSignin();
        }

        this.formSetedUp = true;
    }

    /**
     * Set form to signin
     */
    setFormToSignin() {
        // Form validation setup
        this.signForm = new FormGroup({
            email: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', [Validators.required, Validators.minLength(6)])
        });

        this.formMode = 'signin';
    }

    /**
     * Set the form to signup
     */
    setFormToSignup() {
        // Form validation setup with contact details
        this.signForm = new FormGroup({
            email: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', [Validators.required, Validators.minLength(6)]),
            repeatPassword: new FormControl('', [Validators.required, Validators.minLength(6)])
        },
            {
                validators: this.MatchPassword
            });

        this.formMode = 'signup';
    }

    /**
     * Signin with email in firebase
     */
    signIn(email: string, password: string) {
        // Here code to signin
        console.log('signin', email, password);
        this._firebaseAuth.auth.signInWithEmailAndPassword(email, password)
            .then((res) => { console.log('success signed in', res); this.resetForm(); this.auth.setUser(res.user); })
            .catch((err) => { console.log('err signed in', err); this.showFeedBack(err.message); });

        // this.auth.loginUser();
    }

    /**
     * Signup with email in firebase
     */
    signUp(email: string, password: string) {
        // Here code to signup
        console.log('signup', email, password);
        this._firebaseAuth.auth.createUserWithEmailAndPassword(email, password)
            .then((res) => {
                console.log('success signed up', res);
                this.resetForm(); // Reseting form
                this.auth.setUser(res.user);
                // After signing up, we set user in database also
                this.provider.setUserInDatabase(res.user);
            })
            .catch((err) => { console.log('err signed up', err); this.showFeedBack(err.message); });
    }

    /**
     * Show feedback after doing an operation
     */
    showFeedBack(msg: string) {
        this.msg = msg;
        // In two seconds the messages is hidden again
        setTimeout(() => {
            this.msg = '';
        }, 5000);
    }

    /**
     * To check the passwords are fine
     * @param AC
     * @constructor
     */
    MatchPassword(AC: AbstractControl) {
        const password = AC.get('password').value; // to get value in input tag
        const confirmPassword = AC.get('repeatPassword').value; // to get value in input tag
        if (password !== confirmPassword) {
            AC.get('repeatPassword').setErrors( {MatchPassword: true} );
        } else {
            return null;
        }
    }

    /**
     * Reset the form after a success login/signup
     */
    resetForm() {
        // Reseting form
        this.signForm.reset();
        this.formMode = 'signin'; // After reset, form is in add mode always
    }
}

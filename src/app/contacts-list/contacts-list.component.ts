import { Component, OnDestroy } from '@angular/core';
import { ProviderService } from '../provider.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
    selector: 'app-contacts-list',
    templateUrl: './contacts-list.component.html',
    styleUrls: ['./contacts-list.component.css']
})
export class ContactsListComponent implements OnDestroy {
    contacts; // Contacts array to display contacts in this component
    sub = new Subscription(); // Subscription to close the sub after getting the contacts from providerService

    constructor(private providerService: ProviderService,
                private auth: AuthService) {
        const path = '/users/' + this.auth.getUser().uid + '/contacts';
        console.log(path);
        // Getting the contacts
        this.sub = providerService.getList(path).subscribe(items => {
            this.contacts = items;
            // Updating the current contact list in the app
            this.providerService.updateCurrentContacts(items);
            // console.log(this.contacts);
        });
    }

    /**
     * Destroying subscription after leaving the page
     */
    ngOnDestroy() {
        this.sub.unsubscribe();
    }

}

import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Contact } from './contact.model';

@Injectable()

/**
 * Provider service is in charge to make all databases operations in the app
 */
export class ProviderService {
    private contacts = [];
    private list = ''; // This var will keep the path for the current user's contacts

    private init = false;

    constructor(private db: AngularFireDatabase) {
        console.log('init', this.init);
    }

    /**
     * To know if the service was init in the app (in case that user refresh the app, we need to load contacts again)
     */
    isInit() {
        return this.init;
    }

    /**
     * Updating the current contacts in the app
     * Due to we will use this array to make database operations (delete, update), we need it always fresh
     * This function is used in each component who display the contacts or works with them to keep this array fresh
     * @param items
     */
    updateCurrentContacts(items: any[]) {
        this.contacts = items;
    }

    /**
     * Returning the current contact list
     */
    getCurrentContacts() {
        return this.contacts.slice();
    }

    /**
     * Get contacts
     * @param list
     */
    getList(list: string) {
        this.init = true;
        return this.db.list(list).valueChanges();
    }

    /**
     * Remove contact from database
     * @param list
     * @param index
     */
    removeContact(list: string, index: number) {
        this.contacts.splice(index, 1);
        console.log('contacts', this.contacts);
        this.db.object(list).set(this.contacts);
        return this.contacts.slice();
    }

    /**
     * Adding a contact
     * @param contact
     */
    addContact(contact: Contact) {
        console.log('adding');
        this.contacts.push(contact);
        this.db.object(this.list).set(this.contacts);
        // In this way, add the objtect with an unique key, not as an array element
        // this.db.list(this.list).push(contact);
    }

    /**
     * Edit a contact
     * @param contact
     * @param index
     */
    editContact(contact: Contact, index: number) {
        console.log('editing');
        this.contacts[index] = contact;
        this.db.object(this.list).set(this.contacts);
    }

    /**
     * After signup a new user, we set their datas in the database
     * @param user
     */
    setUserInDatabase(user: any) {
        const userId = user.uid;
        console.log(user);
        // Creating user with its fields in database
        this.db.object('/users/' + userId).set({email: user.email, contacts: ''});
    }

    /**
     * Setting contact path for internal database operations with this service
     * @param path
     */
    setContactPath(path: string) {
        this.list = path;
        console.log('list path', this.list);
    }
}

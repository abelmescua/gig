import { Component } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent {

    constructor(private auth: AuthService,
                private _firebaseAuth: AngularFireAuth) {
    }

    /**
     * To logout the user
     */
    logout() {
        this._firebaseAuth.auth.signOut()
            .then((data) => {
                console.log('logout', data);
                this.auth.logoutUser();
            })
            .catch((err) => { console.log('error in logout', err); });
    }
}

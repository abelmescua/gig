import { Component, OnDestroy } from '@angular/core';
import { ProviderService } from '../provider.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
    selector: 'app-contacts-manager',
    templateUrl: './contacts-manager.component.html',
    styleUrls: ['./contacts-manager.component.css']
})
export class ContactsManagerComponent implements OnDestroy {
    contacts; // Contacts array
    sub = new Subscription(); // Subscription to get the contacts from providerService
    path = ''; // the list url of the user's contacts

    msg = ''; // To give feedback to the user

    constructor(private providerService: ProviderService,
                private auth: AuthService) {
        // User contacts path
        this.path = '/users/' + this.auth.getUser().uid + '/contacts';
        console.log(this.path);
        // Getting the contacts
        this.sub = providerService.getList(this.path).subscribe(items => {
            this.contacts = items;
            // Updating the current contact list in the app
            this.providerService.updateCurrentContacts(this.contacts);
        });
    }

    /**
     * Destroying subscription after leaving the page
     */
    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    /**
     * Remove a contact
     * @param index
     */
    remove(index: number) {
        this.contacts = this.providerService.removeContact(this.path, index);
        this.showFeedBack();
    }

    /**
     * Show feedback to the user
     */
    showFeedBack() {
        this.msg = 'Contact deleted!';
        // In two seconds the messages is hidden again
        setTimeout(() => {
            this.msg = '';
        }, 2000);
    }
}

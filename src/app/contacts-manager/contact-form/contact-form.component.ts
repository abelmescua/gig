import { Component, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Contact } from '../../contact.model';
import { ProviderService } from '../../provider.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../../auth.service';
// country-list library
declare var require: any;
const countries = require('country-list')();

@Component({
    selector: 'app-contact-form',
    templateUrl: './contact-form.component.html',
    styleUrls: ['./contact-form.component.css']
})
export class ContactFormComponent implements OnDestroy {

    // Form vars
    contactForm; // The FromGroup object var who will keep the form configuration
    formMode = 'add'; // Default is 'add' mode
    countries = []; // Countries array to store them
    formSetedUp = false; // To display the html form after the 'FormGroup' is ready in here

    index; // In case that params came to editing some contact, we save the 'index' param from router in this var
    msg = ''; // to show feedback to the user
    path = ''; // User contacts path

    // contact subscription
    sub = new Subscription();

    constructor(private providerService: ProviderService,
                private route: ActivatedRoute,
                private auth: AuthService) {
        this.path = '/users/' + this.auth.getUser().uid + '/contacts';
        console.log(this.path);
        // Getting the countries for the form 'select'
        this.countries = countries.getNames();

        // If user goes to forms and press 'refresh' in order to get the contacts again, we use this code
        if (!this.providerService.isInit()) {
            console.log('loading contacts');
            // Getting the contacts
            this.sub = providerService.getList(this.path).subscribe(items => {
                // Updating the current contact list in the app
                this.providerService.updateCurrentContacts(items);

                // Edit mode
                this.setForm();
            });
        } else {
            // Edit mode
            this.setForm();
        }
    }

    /**
     * Destroying subscription after leaving the page
     */
    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    /**
     * Submit method
     */
    onSubmit() {
        const values = this.contactForm.value;
        console.log(values);
        const contact = new Contact(values.firstName, values.lastName, values.email, values.country);
        console.log('new Contact', contact);

        // Cheking form mode (add || edit)
        if (this.formMode === 'add') {
            // Adding new contact
            this.providerService.addContact(contact);
        } else {
            // Editing current contact
            this.providerService.editContact(contact, this.index);
        }
        // Reseting form
        this.contactForm.reset();
        this.showFeedBack(); // Print feedback message to the user
        this.formMode = 'add'; // After reset, form is in add mode always
    }

    /**
     * Set the form according of the mode that user is coming ('add' or 'edit')
     */
    setForm() {
        // Edit mode
        if (this.route.snapshot.params.index) {
            this.setFormToEdit();

            // Adding mode
        } else {
            this.setFormToAdd();
        }

        this.formSetedUp = true;
    }

    /**
     * Set form to add a contact
     */
    setFormToAdd() {
        // Form validation setup
        this.contactForm = new FormGroup({
            firstName: new FormControl('', Validators.required),
            lastName: new FormControl('', Validators.required),
            email: new FormControl('', [Validators.required, Validators.email]),
            country: new FormControl('Spain', Validators.required)
        });
    }

    /**
     * Set the form to edit a contact
     */
    setFormToEdit() {
        this.index = Number(this.route.snapshot.params.index);
        // Getting the contact by the index that we sent by routerLink
        const contact = this.providerService.getCurrentContacts()[this.index];
        console.log('contact', contact);

        // Form validation setup with contact details
        this.contactForm = new FormGroup({
            firstName: new FormControl(contact.firstName, Validators.required),
            lastName: new FormControl(contact.lastName, Validators.required),
            email: new FormControl(contact.email, [Validators.required, Validators.email]),
            country: new FormControl(contact.country, Validators.required)
        });

        this.formMode = 'edit';
    }

    /**
     * Show feedback after doing an operation
     */
    showFeedBack() {
        if (this.formMode === 'edit') {
            this.msg = 'Contact Edited!';
            // Removing the params to don't edit contact again
            this.route.snapshot.params = [];
        } else {
            this.msg = 'Contact Added!';
        }
        // In two seconds the messages is hidden again
        setTimeout(() => {
            this.msg = '';
        }, 2000);
    }
}

export class Contact {
  constructor(private firstName: string, private lastName: string, private email: string, private country: string) { }
}

import { Component, OnDestroy } from '@angular/core';
import { AuthService } from './auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy{
  online = false;

  // To save the sub of login status changes
  loginSub = new Subscription();

  constructor (private auth: AuthService) {
    // Cheking if user is online or not
    this.online = auth.isOnline();

    // Always listening for changes in user login
    this.loginSub = this.auth.loginChange.subscribe((status: boolean) => {
      console.log('status', status);
      this.online = status;
    });
  }

  ngOnDestroy() {
    this.loginSub.unsubscribe();
  }
}

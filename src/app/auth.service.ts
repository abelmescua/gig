/**
 * AuthService will manage the authentication status in the app
 * When the check if the user is online or not with the authState of firebase package,
 * this service will tell the app to send the user to the login/signup form or to let it go inside the app
 */
import { Subject } from 'rxjs';

export class AuthService {
    private login = false;

    // User var to store user data
    user;

    loginChange = new Subject(); // If user login or logout, we need to let it know to app.component.ts file to manage


    /**
     * To know if user is online or not
     */
    isOnline() {
        return this.login;
    }

    /**
     * Login the user
     */
    loginUser() {
        this.login = true;
        this.sendUpdateStatus();
    }

    /**
     * Logout the user
     */
    logoutUser() {
        this.login = false;
        this.sendUpdateStatus();
    }

    /**
     * When user login or logout, we send update to the app.component.ts
     */
    sendUpdateStatus() {
        this.loginChange.next(this.login);
    }

    /**
     * Setting current login user
     * @param user
     */
    setUser(user: any) {
        this.user = user;
        console.log('user data', user);
    }

    /**
     * Getting the current user
     */
    getUser() {
        return this.user;
    }
}
